<?php

namespace App\Http\Controllers;

use App\Entities\Course;
use App\Entities\Student;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Illuminate\Http\Request;


class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $em;
    private $qb;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        $this->qb = $this->em->createQueryBuilder();
    }

    public function addStudent(Request $request)
    {
        $name = $request->get('name');
        if ($name != null) {
            $this->em->persist(new Student($name));
            $this->em->flush();
        }
        return response()->json($name);
    }

    public function findStudent($id)
    {

        $studentJson = null;

        try {
            $student = \EntityManager::find('App\Entities\Student', $id);
            if ($student != null) {
                $studentJson = ['id' => $student->getId(), 'name' => $student->getFirstName()];
            }
            return response()->json($studentJson);
        } catch (Exception $exception) {
            dd($exception);
        }
        return response()->json("Cannot find Student!");

    }

    public function deleteStudent(Request $request)
    {
        $student = null;
        $id = $request->input('id');
        if ($id != null) {
            $student = \EntityManager::find('App\Entities\Student', $id);
        }
        if ($student != null) {
            \EntityManager::remove($student);
            \EntityManager::flush();
        }

        return response()->json("Delete Successfully");
    }

    public function updateStudent(Request $request)
    {
        $studentId = $request->input("id");
        $name = $request->input("name");
        $student = null;

        if ($studentId != null) {
            $student = \EntityManager::find('App\Entities\Student', $studentId);
        }
        if ($name != null) {
            $student->setFirstName($name);
        }

//        \EntityManager::refresh($student);
        \EntityManager::flush();
        return response()->json($student->getFirstName());
    }

    public function getAllCourses(Request $request)
    {

        $numberOfItems = $request->input('limit', 5);
        $page = $request->input('page', 1);
        $offset = ($page - 1) * $numberOfItems;
        $query = $this->qb->select('course')
            ->from('App\Entities\Course', 'course')
            ->setFirstResult($offset)
            ->setMaxResults($numberOfItems)
            ->getQuery();
        $monHoc = $query->getArrayResult();
        return response()->json($monHoc);
    }

    public function getAllStudents(Request $request)
    {

        $numberOfItems = $request->input('limit', 5);
        $page = $request->input('page', 1);
        $offset = ($page - 1) * $numberOfItems;
        $query = $this->qb->select('student')
            ->from(Student::class, 'student')
            ->setMaxResults($numberOfItems)
            ->setFirstResult($offset)
            ->getQuery();

        $students = $query->getArrayResult();
        return response()->json($students);

    }

    public function getStudentsCourses()
    {

        $query = $this->qb->select('student', 'studentCourses')
            ->from(Student::class, 'student')
            ->join('student.courses', 'studentCourses')
            ->getQuery();

        $result = $query->getArrayResult();
        return response()->json($result);
    }

    public function addCourse(Request $request)
    {
        $name = $request->input('name');
        if ($name != null) {
            \EntityManager::persist(new Course($name));
            \EntityManager::flush();
        }
        return response()->json("Success");
    }

    public function assignCourse(Request $request)
    {
        $courseId = $request->input('courseId');
        $studentId = $request->input('studentId');
        $student = null;
        if ($courseId != null && $studentId != null) {
            $course = \EntityManager::find('App\Entities\Course', $courseId);
            $student = \EntityManager::find('App\Entities\Student', $studentId);
            $student->addCourse($course);
            \EntityManager::flush();
        }

        return response()->json("Success");
    }

}
