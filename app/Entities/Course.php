<?php


namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="course")
 */
class Course
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * @ORM\Column(type="string")
     */
    protected $courseName;

    /**
     * @ORM\ManyToMany(targetEntity="Student", inversedBy="courses")
     */
    protected $students;

    public function __construct($mon)
    {
        $this->courseName = $mon;
        $this->students = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $student
     */
    public function addStudent($student)
    {
        $this->students[] = $student;
    }

    /**
     * @return mixed
     */
    public function getStudent()
    {
        return $this->courseName;
    }
}