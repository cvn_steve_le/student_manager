<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="student")
 */
class Student
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $firstName;

    /**
     * @ORM\ManyToMany(targetEntity="Course", mappedBy="students", cascade={"persist"})
     */
    protected $courses;

    /**
     * Student constructor.
     * @param $firstName
     */

    public function __construct($firstName)
    {
        $this->firstName = $firstName;
        $this->courses = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function addCourse(Course $monHoc){
        $monHoc->addStudent($this);
        $this->courses[] = $monHoc;
    }

    public function getCourse(){
        return $this->courses;
    }

    public function setFirstName($name){
        $this->firstName = $name;
    }

}