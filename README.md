# Student Manager API
 Simple api using Lumen framework to get / insert data from / to database 
 
 # Prerequisite:
 - Nginx
 - Php 7.2
 - Mysql
 - Composer
 
 # Usage:
  - View all students: `http://localhost/students`
  - View all courses: `http://localhost/courses`
  - Add student: `http://localhost/students?name=student_name` `POST` method
  - Add course: `http://localhost/course?name=course_name` `POST` method
  - Add course to student `http://localhost/students/course?student_id=?course_id=` `PUT` method
