<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('hello',function (){
    return "Hello, World";
});

$router->get('students/course', 'ExampleController@getStudentsCourses');

$router->get('students', 'ExampleController@getAllStudents');

$router->get('students/{id}', 'ExampleController@findStudent');

$router->get('course', 'ExampleController@getAllCourses');

$router->post('students', 'ExampleController@addStudent');

$router->put('students', 'ExampleController@updateStudent');

$router->delete('students', 'ExampleController@deleteStudent');

$router->post('course', 'ExampleController@addCourse');

$router->put('course', 'ExampleController@assignCourse');
